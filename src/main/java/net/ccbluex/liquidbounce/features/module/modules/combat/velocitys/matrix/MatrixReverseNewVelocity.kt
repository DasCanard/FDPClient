package net.ccbluex.liquidbounce.features.module.modules.combat.velocitys.matrix

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.modules.combat.velocitys.VelocityMode
import net.ccbluex.liquidbounce.features.value.BoolValue
import net.ccbluex.liquidbounce.features.value.FloatValue
import net.ccbluex.liquidbounce.features.value.IntegerValue

class MatrixReverseNewVelocity : VelocityMode("MatrixTest") {
    private val longJumpMode = BoolValue("Matrix-Longjump-Test", false)
    private val smoothReverseStrengthValue = FloatValue("ReverseStrength", 0.10F, 0.05F, 0.19F).displayable { longJumpMode.get() }
    private val timePassing = IntegerValue("TimePassing(MS)", 20, 10, 80).displayable { longJumpMode.get() }
    private var reverseHurt = false

    override fun onEnable() {
        reverseHurt = false
    }

    override fun onVelocityPacket(event: PacketEvent) {
        velocity.velocityInput = true
    }

    override fun onVelocity(event: UpdateEvent) {
        if (!velocity.velocityInput) {
            mc.thePlayer.speedInAir = 0.02F
            return
        }

        if (mc.thePlayer.hurtTime > 0) {
            reverseHurt = true
        }

        if (!mc.thePlayer.onGround) {
            if (reverseHurt) {
                when {
                    longJumpMode.get() -> {
                        mc.thePlayer.speedInAir = smoothReverseStrengthValue.get()
                    }
                    else -> {
                        mc.thePlayer.speedInAir = 0.04F
                    }
                }
            }
        } else if (velocity.velocityTimer.hasTimePassed(timePassing.get().toLong())) {
            velocity.velocityInput = false
            reverseHurt = false
        }
    }
}